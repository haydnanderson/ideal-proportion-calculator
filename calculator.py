import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

data = {
  "weight_height_ratio": [1.899,2.038,2.183,2.334,2.489,2.650,2.817,2.989,3.195,3.379],
  "measurements": {
    "chest": [36.4,37.9,39.3,40.7,42.1,43.5,45,46.4,48,49.4],
    "biceps": [13.1,13.6,14.1,14.6,15.1,15.7,16.2,16.7,17.3,17.8],
    "forearm": [10.9,11.3,11.8,12.2,12.6,13.1,13.5,13.9,14.4,14.8],
    "waist": [27.3,28.4,29.4,30,31.6,32.6,33.7,34.8,36,37.1],
    "neck": [14,14.5,15,15.6,16.1,16.7,17.2,17.8,18.3,18.8],
    "hips": [32.8,34.1,35.3,36.6,37.9,39.2,40.5,41.7,43.2,44.5],
    "thighs": [19.7, 20.4,21.2,22,22.7,23.5,24.3,25,25.9,26.7]
  }
}

def find_x(y_val, coef, intercept):
  return (y_val-intercept)/coef

def find_y(x_val):
  for key, item in data['measurements'].items():
    slope, intercept = calculate_coef(item)
    measurement = (slope*x_val)+intercept
    print(f"[{key}] {round(measurement, 2)} in.")

def calculate_coef(array):
  x = np.arange(len(array))
  y=np.array(array)
  model = LinearRegression(fit_intercept=True)

  model.fit(x[:, np.newaxis], y)

  xfit = np.linspace(0, 10, 1000)
  yfit = model.predict(xfit[:, np.newaxis])
  # print("Model slope:    ", model.coef_[0])
  # print("Model intercept:", model.intercept_)
  return(model.coef_[0], model.intercept_)

def run(height, weight):
  ratio = weight/height
  slope, intercept = calculate_coef(data['weight_height_ratio'])
  x_val = find_x(ratio, slope, intercept)
  find_y(x_val)

# Calculate Coef
# Find x value of your body weight
# loop over measurements to calculate y values

height = 64 # in
weight = 105 # lb

run(height, weight)
